
local vim = vim

vim.g.mapleader = " "
vim.g.maplocalleader = " "

vim.g.have_nerd_font = true

vim.opt.autoread = true
vim.opt.tabstop = 8
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.autoindent = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.wrap = false
vim.opt.smartcase = true
vim.opt.ignorecase = true
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.writebackup = false
vim.opt.incsearch = true
vim.opt.mouse = "a"
vim.opt.signcolumn = "yes"
vim.opt.list = true
vim.opt.listchars = { tab = '»‧', trail = '·', nbsp = '␣' }
vim.opt.updatetime = 250
vim.opt.inccommand = "split" -- live substitution preview
vim.opt.background = "dark"
vim.opt.showmode = false
vim.opt.scrolloff = 10

vim.opt.undofile = true
vim.opt.undodir = vim.fn.expand("$HOME/.nvim/undodir")

local keymap_options = { noremap =  true }

vim.keymap.set("n", "<Esc>", "<cmd>nohlsearch<CR>", keymap_options) -- clear search with esc

require("config.lazy")

require("lualine").setup({
  options = {
    theme = "gruvbox"
  }
})

require("config.telescope")

require("config.cmp")
require("config.lsp")

