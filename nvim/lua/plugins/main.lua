
return {
  {
    "ellisonleao/gruvbox.nvim",
    priority = 1000,
    lazy = false,
    config = function ()
      require("gruvbox").setup({ contrast = "" })
      vim.cmd([[colorscheme gruvbox]])
    end,
  },
  {
    'nvim-lualine/lualine.nvim',
    dependencies = { 'nvim-tree/nvim-web-devicons' }
  },
  { "airblade/vim-gitgutter" },
  {
    "folke/lazydev.nvim",
    ft = "lua",

  },
  {
    'nvim-telescope/telescope.nvim',
    lazy = true,
    dependencies = {
        {'nvim-lua/plenary.nvim'},
    }
  },
  { "neovim/nvim-lspconfig" },
  {
    "hrsh7th/nvim-cmp",
    event = "InsertEnter",
    dependencies = {
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-cmdline",
    },
    opts = function (_, opts)
      opts.sources = opts.sources or {}
      table.insert(opts.sources, {
        name = "lazydev",
        group_index = 0,

      })
    end,
  },
  {
    "windwp/nvim-autopairs",
    event = "InsertEnter",
    config = true
  },
  {
    "williamboman/mason.nvim",
      dependencies = {
        "williamboman/mason-lspconfig.nvim", -- auto install lsps
        "WhoIsSethDaniel/mason-tool-installer.nvim",
    }
  }, -- lsp installer
  { "ziglang/zig.vim" },
}

