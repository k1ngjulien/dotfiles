
local cmp = require('cmp')
cmp.setup({
   snippet = {
    -- REQUIRED - you must specify a snippet engine
    expand = function(args)
      vim.snippet.expand(args.body) -- For native neovim snippets (Neovim v0.10+)
    end,
  },
  window = {
    completion = {
      scrolloff =  5
    },
    documentation = {
      scrolloff =  5
    },
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<CR>'] = cmp.mapping.confirm({ select = true, behavior = cmp.ConfirmBehavior.Insert }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.

    ['<C-j>'] = cmp.mapping.select_next_item(),
    ['<C-k>'] = cmp.mapping.select_prev_item(),

    -- TODO: move vim.snipped config  somewhere else
    ['<Tab>'] = cmp.mapping(function(fallback)
      if vim.snippet.active({ direction = 1}) then -- next field in snippet
        vim.snippet.jump(1)
      elseif cmp.visible() then -- when open, go to next completion
        cmp.select_next_item()
      else
        fallback()
      end
    end, { 'i', 's'}),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if vim.snippet.active({ direction = -1}) then -- prev field in snippet
        vim.snippet.jump(-1)
      elseif cmp.visible() then -- when open, go to prev completion
        cmp.select_prev_item()
      else
        fallback()
      end
    end, { 'i', 's'}),
  }),

  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
  }, {
    { name = 'buffer' },
  })
})

local cmp_autopairs = require('nvim-autopairs.completion.cmp')
cmp.event:on(
  'confirm_done',
  cmp_autopairs.on_confirm_done()
)

-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore
cmp.setup.cmdline({ '/', '?' }, {
  mapping = cmp.mapping.preset.cmdline(),
  sources = {
    { name = 'buffer' }
  }
})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(':', {
  mapping = cmp.mapping.preset.cmdline(),
  sources = cmp.config.sources({
    { name = 'path' }
  }, {
    { name = 'cmdline' }
  }),
  matching = { disallow_symbol_nonprefix_matching = false }
})
