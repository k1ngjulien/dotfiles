# K1ngjulien bashrc

export HISTSIZE=10000
 
alias ls="ls --color=auto --group-directories-first"
alias la="ls -lAh"
alias lag="lsg -lAh"
alias v="nvim"
alias dps="docker ps -a --format 'table {{.Names}}\t{{.Status}}\t{{.Mounts}}\t{{.Ports}}\t{{.Image}}'"
alias glog="git log -25 --oneline"
alias gstat="git status"
alias gpush="git push"
alias gpull="git pull --rebase"
alias gtree="git log --oneline --graph --decorate --all"
alias gsmu="echo 'updating submodules' && git submodule update --init --recursive && echo 'update complete'"
alias xclip="xclip -selection 'clipboard'"
alias yay-update="echo y | LANG=C yay --noprovides --answerdiff None --answerclean None --mflags '--noconfirm' -Syu"

alias platformio="~/.platformio/penv/bin/platformio"
alias reboot="shutdown -r now"

alias tb="nc termbin.com 9999"

export EDITOR=nvim
export PATH=$PATH:~/go/bin:~/dotfiles/bin:~/opt/bin
export MAKEFLAGS="-j$(nproc --ignore=2)"

# work
alias fkvpn="sudo openvpn ~/Documents/flightkeys.ovpn"
export FLIGHTKEYS_CONFIG_API=http://service-config:15020/service-config/;
export FLIGHTKEYS_NAMESPACE=fkydev

function is_interactive_shell() {
  # https://www.gnu.org/software/bash/manual/html_node/Is-this-Shell-Interactive_003f.html
  [[ "$-" =~ "i" ]]
}


if is_interactive_shell; then
  stty -ixon
  bind 'TAB:menu-complete'
  bind 'set show-all-if-ambiguous on'
  bind 'set completion-ignore-case on'
  bind 'set completion-map-case on' # _ and - are equal in completion
  bind '"\C-s":"\e[1~sudo \e[4~"' # ctrl+s inserts sudo at the start
  bind '"\C-h":"\e[1~cht \e[4~"' # ctrl+h inserts cht at the start
fi

# import fzf features like better ctrl+r search
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# run ls after every cd
function cd {
    builtin cd "$@" && ls
}

#make manpages coloured
man() {
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man "$@"
}

# parse out git status when inside a git repository
git_status_display() {
    branch=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1/')
    
    # check if in git repository
    if [ ! -z "$branch" ] 
    then
       printf " $branch"

       numchanges=$(git status -s 2>/dev/null)

       # check if changed files exist
       if [[ $? -eq 0 ]]
       then
           numchanges=$(git status -s | wc -l)
           if [[ $numchanges -ne 0 ]]
           then
               printf ":$numchanges"
           fi
       fi

       printf ")"
    fi
}

# get name of active pytho venv
function python_venv_info(){
    # Get Virtual Env
    if [[ -n "$VIRTUAL_ENV" ]]; then
        # Strip out the path and just leave the env name
        venv="${VIRTUAL_ENV##*/}"
    else
        # In case you don't have one activated
        venv=''
    fi
    [[ -n "$venv" ]] && echo " (venv:$venv)"
}

# disable the default virtualenv prompt change
export VIRTUAL_ENV_DISABLE_PROMPT=1

# Share history between multiple terminals:
# Append after every command and reload it on every new prompt
# This means you might have to press enter once to refresh the history
shopt -s histappend
export PROMPT_COMMAND="__prompt_command; history -a;history -n"

__prompt_command() {
    # capture last exit code
    local EXIT="$?"

    PS1=""
    local White='\[\e[0m\]'

    local Red='\[\e[0;31m\]'
    local Gre='\[\e[0;32m\]'
    local Blu='\[\e[0;96m\]'

    PS1="${White}["

    # show hostname when inside an ssh session
    if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
        if [ -z $TMUX ]; then
            PS1+="${White}\H "
        else
            PS1+="${White}+ "
        fi
    fi

    # show active python venv
    PS1+="${Blu}\w"
    PS1+="${White}\$(python_venv_info)";

    # git status
    PS1+="${Gre}\$(git_status_display)"

    # exit codes
    if [ $EXIT != 0 ]; then
        PS1+=" ${Red}(${EXIT})"
    fi

    PS1+="${White}]: "
}

if command -v direnv &> /dev/null
then
  eval "$(direnv hook bash)"
fi



# more scripts

source ~/dotfiles/kubectlcompletion.sh

